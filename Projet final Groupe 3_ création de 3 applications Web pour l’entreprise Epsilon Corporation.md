# Projet final Groupe 3: création de 3 applications Web pour l'entreprise Epsilon Corporation

- Sommaire
[ToC]

## 1) Cahier des charges
Pour répondre à la demande du projet, 4 serveurs seront créés. Les serveurs doivent répondre au cahier des charges suivant: 
### Un serveur application : 
Ce serveur sera constitué des applications demandées c’est à dire le blog jekyll, l’application JAVA tomcat, ainsi que l’application python. La haute disponibilité de ce serveur nécessite une connexion NFS au serveur NFS pour toutes les données configuration applications, site web, et base de données. **Distribution retenue : CentOS 7.**
### Un serveur outils: 
Ce serveur sera constitué des applications métiers, c'est-à-dire GitLab, Jenkins et Ansible. Le serveur nécessite une connexion NFS au serveur NFS pour tout ce qui est configuration des services. En effet, la conteneurisation de Jenkins et Gitlab demandera un backup des configurations sur le serveur NFS pour un redéploiement plus rapide.**Distribution retenue : Debian 10.** 
### Un serveur NFS: 
Un serveur contenant toutes les données sensibles des autres serveurs (site web, configuration, base de données). Afin de sécuriser les données, ce serveur aura un RAID 5. **Distribution retenue : CentOS 7.**
### Un serveur frontend: 
Ce serveur sera un reverse proxy fonctionnant avec NGINX, redirigeant si possible vers toutes les applications des autres serveurs (gitlab, jenkins, jekill, python …). **Distribution retenue : Debian 10.**

![](https://i.imgur.com/mvDiUhF.png)





## 2) Provisioning de serveurs sur AWS via ansible
**Pour commencer**, on a créé un user dans aws grâce au service IAM, l’user a une politique admin (comme vu en cours) et nous retourne : 
* Une ID
* Une clef secrète

Ces deux identifiants sont enregistrés dans un fichier `my_key.yml` sous forme de variable YAML pour ansible et serviront donc au provisionning des instances aws EC2. 

Avant toute chose, il est nécessaire de préparer le terrain… C'est-à-dire, créer une élastic IP sur aws, créer un VPC avec ses subnets et enfin créer des groupes de sécurité. **A savoir que la zone choisie est Milan.** 

### a) Creation Elastic IP : 
* Sur l’onglet à gauche dans le service EC2 cliquez sur “Adresses IP Elastic”
* Ensuite cliquez sur “Allouer l'adresse IP Elastic”
* Sélectionnez “Pool d'adresses IPv4 Amazon”
* Pas d'accélérateur, pas de balise
* Finir par “Allouer”

*Gardez l’id de l’elastic IP créé elle sera nécessaire pour la création du VPC*

### b) Création du VPC:
* Recherchez le service VPC
* Cliquez sur “lancer l’assistant VPC”

*Pour l’instant la mise en place d’un VPN est mis de côté et sera peut être ajouté à posteriori si possible*

* Cliquez sur “VPC avec des sous-réseaux publics et privés”

#### Paramétrage VPC: 
* **Bloc d'adresse CIDR IPv4:** *laissez identique*
* *Pas de bloc d'adresse CIDR IPv6*
* **Nom du VPC:** *Groupe3_VPC*
* **Le bloc d'adresse CIDR IPv4 du sous-réseau public:** *laissez identique*
* **Zone de disponibilité:** *eu-south-1b*
* **Nom du sous-réseau public:** *Le Nom souhaité*
* **Bloc CIDR IPv4 du sous-réseau privé:** *laissez identique*
* **Zone de disponibilité:** *eu-south-1b*
* **Nom du sous-réseau privé:** *Le Nom souhaité*
* **ID d'allocation d'adresses IP Elastic:** *L’ID de l’elastic IP créée*
* **Points de terminaison de service:** *Laissez vide*
* **Activer les noms d'hôte DNS:** *oui*
* **Location matérielle:** *par défaut*

### c) Création des groupes de sécurité:
* Dans le service EC2 cliquez sur “groupe de sécurité" dans le volet de navigation à gauche
* Cliquez sur “Créer un groupe de sécurité”
#### Paramétrage groupe de sécurité:
* **Nom du groupe de sécurité** : *Le nom d’un des serveurs*
* **Description** : *Une description du groupe de sécurité*
* **VPC** : *L’ID du VPC créé précédemment*
* **Règles entrantes** : *Ajoutez les règles nécessaires pour le bon fonctionnement du serveur. Ex : SSH port 22 0.0.0.0/0 pour l’administration des serveurs par ansible*
* **Règles sortantes** : *Pour l’instant, à laisser tel quel*
* *Pas de balises*

**L'opération de création d’un groupe de sécurité est répétée pour chaque serveur.**

### d) Provisioning via ansible
C’est à partir de là que commence le “vrai” provisioning… Tout d’abord il faut sécuriser les IDs enregistrés dans le fichier `my_key.yml`. Ceci se fait grâce à la commande : 
* `ansible-vault encrypt my_key.yml`

Ensuite il est nécessaire d’initialiser ses rôles, ceci se fait grâce à la commande: 
* `ansible-galaxy init “nom_du_roles”`

Un script rôle dynamique ansible type est créé pour chaque serveur, voici le provisionning type établi par exemple pour le serveur frontend :
```yaml
    - name: Creation de clef
      ec2_key:
        aws_access_key: "{{ access_key }}"
        aws_secret_key: "{{ secret_key }}"
        name: "{{ key_name }}"
        region: "{{ region }}"
      register: ec2_key_result

    - name: Save private key
      copy: content="{{ ec2_key_result.key.private_key }}" dest="/root/.ssh/aws.key_frontend" mode=0600
      when: ec2_key_result.changed

    - name: Creation de l'instance
      ec2:
        aws_access_key: "{{ access_key }}"  
        aws_secret_key: "{{ secret_key }}"
        instance_type: t3.medium
        image: "{{ image }}"
        key_name: "{{ key_name }}"
        count: 1
        group: “{{ groupe_secu }}”
        region: "{{ region }}"
        wait: yes       
        vpc_subnet_id: "{{ subnet }}"
        assign_public_ip: yes
        instance_tags:
           Name: "{{ name_instance }}"
      register: ec2
    
    - name: Save IP to inventory file
      lineinfile:
       dest: "{{hosts_file}}"
       insertafter: '\[frontend\]'
       line: "{{item.public_ip}}"
      with_items: "{{ec2.instances}}"
```
les variables créés dans default sont les suivantes : 
```yaml  
region: eu-south-1 -> Identique pour chaque serveur  
image: “L’ID de l’ami (CentOS 7 ou Debain 10)  
key_name: “Le nom de clef ssh correspondant au serveur”  
name_instance: “Le nom du serveur”   
groupe_secu: “Le nom du groupe de sécurité”  
subnet: subnet-076b5c2d1f7e1ee4a -> Identique au début pour chaque serveur  
hosts_file: /root/projet_final_groupe3/hosts -> Identique pour chaque serveur
```

Une fois chaque rôle créé, le playbook principal est lancé. Via la commande :
* `ansible-playbook mon_playbook.yml --ask-vault-pass`

Voici le playbook de provisioning principal : 
``` yaml
- hosts: localhost
   connection: local
   gather_facts: False
   vars_files:
      - my_key.yml

   tasks:
   roles:
      - frontend
      - server_nfs
      - server_service
      - server_outils
```      

## 3) Deploiement du serveur outils
Le deploiement du serveur outils se fait via un playbook ansible, ce playbook deploira docker sur le serveur via un role "install_docker_debian".

Un role "outils" deploie deux conteneurs : Gitlab et Jenkins. Le role "outils" deploie Ansible de façon classique, c'est à dire via un package.

### Role "install_docker_debian"

``` yaml
# tasks file for docker
- name: Installation des packages necessaires pour l'installation de docker 
  package:
    name: 
        - ca-certificates
        - curl
        - gnupg
        - gnupg2
        - lsb-release
        - wget
        - software-properties-common
        - python-pip
        - python-docker
    state: present
    update_cache: yes
    
- name: ajout apt key 
  apt_key:
    url: https://download.docker.com/linux/debian/gpg
    state: present

- name: ajout dépot docker officiel
  apt_repository:
    repo: "deb [arch=amd64] https://download.docker.com/linux/debian stretch stable"
    state: present

- name: installation de docker
  apt:
    name:
       - docker-ce
       - docker-ce-cli
       - containerd.io  
    update_cache: yes
    state: present

- name: demarre et active docker
  service:
    name: docker
    state: started
    enabled: True
```

### Role "outils"

``` yaml
# tasks file for gitlab
- name: installation nfs-common
  apt:
    name: nfs-common
    state: present

- name: montage volume NFS
  mount:
    src: 15.161.108.88:/nfs-share/exports/server_outils
    path: /srv
    opts: rw,sync,hard,intr
    state: mounted
    fstype: nfs

- name: Create gitlab directories
  file: path={{ item }} recurse=yes state=directory
  with_items:
    - "{{ gitlab_config_dir }}"
    - "{{ gitlab_logs_dir }}"
    - "{{ gitlab_data_dir }}"
    - "{{ gitlabrunner_dir2 }}"


- name: Install Gitlab container
  docker_container:
    name: gitlab
    image: "{{ gitlab_image }}"
    state: started
    ports:
      - "{{ gitlab_https_port }}:443"
      - "{{ gitlab_http_port }}:80"
      - "{{ gitlab_ssh_port }}:22"
    volumes:
      - "{{ gitlab_config_dir }}:/etc/gitlab"
      - "{{ gitlab_logs_dir }}:/var/log/gitlab"
      - "{{ gitlab_data_dir }}:/var/opt/gitlab"
    restart_policy: always

- name: rm conf gitlab file
  file:
    path: "{{ gitlab_config_dir }}/gitlab.rb"
    state: absent

- name: deploy gitlab configuration
  template: src=gitlab.rb.j2 dest="{{ gitlab_config_dir }}/gitlab.rb" owner=root group=root mode=0644

- name: Run a simple command (command)
  shell:
    cmd: sudo docker exec gitlab gitlab-ctl reconfigure
    
- name: Install Gitlab-runner container
  docker_container:
    name: gitlab-runner
    image: gitlab/gitlab-runner:latest
    state: started
    volumes:
      - "{{ gitlabrruner_dir1 }}:/var/run/docker.sock"
      - "{{ gitlabrunner_dir2 }}:/etc/gitlab-runner"

- name: Create jenkins directory
  file: path="{{ jenkins_dir }}" recurse=yes state=directory owner=admin group=admin

- name: Install Jenkins container
  docker_container:
    name: jenkins
    image: "{{ jenkins_image }}"
    state: started
    ports:
      - "{{ jenkins_port1 }}:8080"
      - "{{ jenkins_port2 }}:50000"
    volumes:
      - "{{ jenkins_dir }}:/var/jenkins_home"
      - /var/run/docker.sock:/var/run/docker.sock

- name: Run a simple command (command)
  shell:
    cmd: echo "deb http://ppa.launchpad.net/ansible/ansible/ubuntu focal main" >> /etc/apt/sources.list

- name: Run a simple command (command)
  shell:
    cmd: sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367

- name: install ansible and update
  apt:
    name: ansible
    update_cache: yes
    state: present
```

### Variables associées au playbook :  
``` yaml
gitlab_config_dir: "/srv/gitlab/config"
gitlab_logs_dir: "/srv/gitlab/logs"
gitlab_data_dir: "/srv/gitlab/data"
gitlab_image: "gitlab/gitlab-ee:latest"
gitlab_ssh_port: "2222"
gitlab_https_port: "443"
gitlab_http_port: "80"

gitlabrruner_dir1: "/var/run/docker.sock"
gitlabrunner_dir2: "/srv/gitlab-runner/config"

jenkins_image: jenkins/jenkins:lts
jenkins_port1: "8080"
jenkins_port2: "50000"
jenkins_dir: "/srv/jenkins"
```

### Descriptif de l'excution du playbook  et taches liées au deploiement :
Le playbook ajoute en premier temps la partie client du serveur NFS et crée un point de montage sur le dossier /nfs-share/exports/server_outils du serveur outils.

Ensuite, le playbook lance un conteneur gitlab avec les volumes associé : config, data et log. Cette parie deploie aussi un fichier de configuration gitalb.rb dans le volumes partagé avec le conteneur : /srv/gitlab/config. Trois projets seront ensuite créés sur le gitlab : un projet projet (regroupant tout les playbook, roles etc du projet), un projet jekyll (ou la partie gitlab ci sera executée) et un projet python (ou la partie serveur web python lié à jenkins sera executée).

Fichier de configuration gitlab.rb
```bash
external_url 'http://15.161.97.81/'
gitlab_rails['lfs_enabled'] = true
gitlab_rails['gitlab_shell_ssh_port'] = 2222

nginx['listen_port'] = 80
nginx['listen_https'] = false
```

Ensuite, le playbook lance le conteneur gitlab-runner. Un runner et en effet nécessaire pour la mise en place de l'integration continue sur gitlab. Le conteneur est lié à deux volumes : un volume de configuration (/etc/gitlab-runner), et un volume permettant de lier le service docker de la machine hote au conteneur (/var/run/docker.sock). La mise en place du runner se fait via la suite de commande suivante sur le serveur outils :  
```bash
docker exec -it gitlab-runner /bin/bash #pour entrer dans le conteneur
gitlab-runner register
```
Ensuite, on renseigne les informations necessaires pour la mise en place du runner de la façon suivante :
```bash
root@d28956c41cbb:/# gitlab-runner register
Runtime platform                                    arch=amd64 os=linux pid=74 revision=4b9e985a version=14.4.0
Running in system-mode.                            
                                                   
Enter the GitLab instance URL (for example, 

https://gitlab.com/): http://15.161.97.81/

Enter the registration token: 4fzHt5Sgu-B3ywN41UzQ

Enter a description for the runner: [d28956c41cbb]: exemple de description

Enter tags for the runner (comma-separated): exemple de tag

Registering runner... succeeded                     runner=4fzHt5Sg

Enter an executor: custom, docker, docker-ssh, parallels, docker+machine, docker-ssh+machine, kubernetes, shell, ssh, virtualbox: docker
```
*L'adresse IP du gitlab et le token sont renseignés sur le compte gitlab créé* **Sur gitlab dans les pamamètres du runner sur "edit", il faut cocher "Indicates whether this runner can pick jobs without tags"**

Ensuite, playbook lance le conteneur jenkins avec les volumes associé data et docker.sock (comme pour gitlab-runner).

Finalement, le playbook ajoute une ligne au sources.list afin d'ajouter le depot pour l'installation d'ansible sur le serveur outils.


## 4) Déploiement du Serveur Applications 

### Rôle install-Tomcat, Phpmyadmin et Mysql

l'application Tomcat, Phpmyadmin ainsi Mysql se fait *via* un playbook ansible.

En effet, le playbook va déployer à distance trois conteneurs dans le serveur application: 
* Un conteneur Tomcat
* Un conteneur Phpmyadmin
* Un conteneur Mysql

### Rôle "Application"

``` yaml
---
# tasks file for appli
- name: installation nfs-utils
  yum:
    name: nfs-utils
    state: present

- name: montage volume NFS
  mount:
    src: 15.161.108.88:/nfs-share/exports/server_appli
    path: /srv
    opts: rw,sync,hard,intr
    state: mounted
    fstype: nfs

- name: Installation des packages 
  yum: 
    name: git
    state: present

- name: Créer des utilisateurs sur CentOS
  user:
    name: "{{ item }}"
    state: present
    shell: /usr/bin/bash
    password: "{{ 'stage' | password_hash('sha512') }}"
  with_items: "{{ user }}"

- name: Create ssh directories
  file: path=/home/{{ item }}/.ssh recurse=yes state=directory owner={{ item }} group={{ item }}
  with_items: "{{ user }}"

- name: copy authorized_file file
  copy:
    src: "/home/centos/.ssh/authorized_keys"
    dest: "/home/{{ item }}/.ssh/authorized_keys"
    remote_src: yes
  with_items: "{{ user }}"

- name: Create project directory
  file: path=/home/{{ item }}/project recurse=yes state=directory owner={{ item }} group={{ item }}
  with_items: "{{ user }}"

- name: suppression du dossier .git
  file:
    path: /home/{{ item }}/project/.git
    state: absent
  with_items: "{{ user }}" 

- name: git init
  shell: 
    cmd: git init
    chdir: /home/{{ item }}/project/
  with_items: "{{ user }}"

- name: droit du dossier project
  file:
    path: /home/{{ item }}/project
    owner: "{{ item }}"
    group: "{{ item }}"
    recurse: yes
  with_items: "{{ user }}"

- name: git remote origin
  shell:
    cmd: git remote add origin {{ item.depot }}
    chdir: /home/{{ item.name }}/project/
  with_items:
    - { name: "{{ user[0] }}", depot: "{{ depot_git[0] }}"}
    - { name: "{{ user[1] }}", depot: "{{ depot_git[1] }}"}

- name: "Creating Directory"
  file:
    path: "{{ item }}"
    state: directory
  with_items: 
    - "{{ mysql_dir1 }}"
    - "{{ mysql_dir2 }}"
    - "{{ tomcat_dir1 }}"
    - "{{ httpd_dir }}"

- name: chown /srv
  shell: 
    cmd: sudo chown -R root:centos /srv
- name: chmod web
  shell: 
    cmd: sudo chmod -R 775 /srv/web

- name: "Install httpd container"
  docker_container:
    name: httpd
    image: httpd
    state: started
    ports: "80:80"
    volumes: "{{ httpd_dir }}:/usr/local/apache2/htdocs/"

- name: copy war file
  copy: 
    src: "target/"
    dest: "{{ tomcat_dir1 }}"

- name: copy sql file
  copy: 
    src: "USER.sql"
    dest: "{{ mysql_dir2 }}"

- name: Install container mysql
  docker_container:
    name: db
    image: mysql:5.7
    volumes:
       - "{{ mysql_dir1 }}:/var/lib/mysql"
       - "{{ mysql_dir2 }}:/docker-entrypoint-initdb.d"
    env:
      MYSQL_ROOT_PASSWORD: root
      MYSQL_DATABASE: testdb1
      MYSQL_USER: testuser
      MYSQL_PASSWORD: stage@groupe3
    ports:
       - "{{ port_mysql }}:3306"

- name: chown mysql
  shell: 
    cmd: sudo docker exec db chown -R mysql:mysql /var/lib/mysql 

- name: Install container Phpadmin
  docker_container:
    name: phpadmin
    image: phpmyadmin/phpmyadmin
    links: db
    ports:
      - "{{ port_phpadmin }}:80"
    env:
      PMA_HOST: db
      MYSQL_ROOT_PASSWORD: root

- name: Install container tomcat
  docker_container:
    name: tomcat
    image: tomcat
    links: db
    volumes:
      - "{{ tomcat_dir1 }}:/usr/local/tomcat/webapps/"
    ports:
      - "{{ port_tomcat }}:8080"
    env:
      MYSQL_ROOT_PASSWORD: root
      MYSQL_DATABASE: testdb1
      MYSQL_USER: testuser
      MYSQL_PASSWORD: stage@groupe3

- name: copy src file python server
  copy:
    src: target_python/
    dest: /home/userpython/project/
    owner: userpython
    group: userpython
    mode: 775
    directory_mode: yes

- name: copy Dockerfile pyhton server
  copy:
    src: "Dockerfile"
    dest: /home/userpython/project/
    owner: userpython
    group: userpython
    mode: 755
    directory_mode: yes

- name: copy Jenkinsfile 
  copy:
    src: "Jenkinsfile"
    dest: /home/userpython/project/
    owner: userpython
    group: userpython
    mode: 755
    directory_mode: yes

```

### Variables associées au Playbook


``` yaml
---
# defaults file for appli
user: 
    - "userjekyll"
    - "userpython"
depot_git:
    - http://15.161.97.81/Groupe3_AstridCyliaRobinColas/projet_jekill.git
    - http://15.161.97.81/Groupe3_AstridCyliaRobinColas/projet_python.git

httpd_dir: "/srv/web/"
port_httpd: "80"

mysql_dir1: "/srv/mysql/mysql"
mysql_dir2: "/srv/mysql/mysql-dump/"
port_mysql: "3306"

port_phpadmin: "8081"

tomcat_dir1: "/srv/tomcat/webapps/"
tomcat_dir2: "/srv/tomcat/conf"
port_tomcat: "8082"

```
### Descriptif du Playbook


Dans un 1er temps, le playbook va lancer l'installation du package NFS qui va permettre la création d'un point de montage sur le dossier nfs-share/exports/server_appli.

Par la suite, la Playbook va créer des utilisateurs au niveau du serveur applications.

La clé permettont aux utilisateurs de se connecter en ssh au serveur application est stockée dans le repertoire ssh de chaque utisateur (*copy authorized_file file*).

La Playbook va se charger de la creation d'un dossier appelé "*project*", les droits de lecture, d'écriture et d'exécution sont accordés aux utilisateurs créés. 

Par la suite, le playbook va lancer le conteneur httpd, ensuite il lancera un conteneur qui permet d'installer MySQl.

Un autre conteneur installera Phpadmin. Ce dernier va être utilisé pour accéder à la base de données. 
En effet, une fois Phpadmin installé, nous pouvons y accéder *via* l'interface web en utilisant l'adresse ip suivi du numéro de port attribué à Phpadmin (15.161.53.65:8081).

![](https://i.imgur.com/e26uEUX.png)


![](https://i.imgur.com/obh4tdK.png)


Le dernier conteneur installera de son coté Tomcat, on peut accéder au site en utilisant l'ip et le port dédié à cette application (12.161.53.65:8082/Nom_du_dossier_des_pages_web).

![](https://i.imgur.com/h7GoKYp.png)

### Configuration de gitlab ci
Un fichier .gitlab-ci.yml est deployé à la racine du gitlab jekyll : 
```yaml=
image: ruby:2.6

variables:
  JEKYLL_ENV: production
  LC_ALL: C.UTF-8

cache:
  paths:
    - vendor/

before_script:
  - gem install bundler
  - bundle install --path vendor

pages:
  stage: deploy
  script:
    - bundle exec jekyll build -d public
    - echo "$SSH_KEY" > ~/jenkins2.pem
    - chmod 600 ~/jenkins2.pem
    - scp -r -i ~/jenkins2.pem -o StrictHostKeyChecking=no public/* centos@15.161.53.65:/srv/web
  artifacts:
    paths:
      - public
  only:
    variables:
      - $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```
Ensuite, une variable SSH_KEY est créée dans settings > CI/CD > Variables :  
* La clé ssh du serveur appli est copié-collé sur la variable et est paramatré en "variable".

Voila... un commit sur le depot déclenchera le gitlab CI. 


### Configuration de jenkins
Un fichier jenkin files est deployé sur le depot git à la racine :  
```jason
node {
    def app

    stage('Clone repository') {
        /* Let's make sure we have the repository cloned to our workspace */

        checkout scm
    }

    stage('Initialize'){
        def dockerHome = tool 'docker'
        env.PATH = "${dockerHome}/bin:${env.PATH}"
    }

    stage('Build image') {
        /* This builds the actual image; synonymous to
         * docker build on the command line */

        app = docker.build("python/server")
    }
}
```
Module et plugin à installer dans jenkins :  
* Docker Commons
* Docker Pipeline
* Docker API
* Docker
* docker-build-step
* gitlab

Tout d'abord il est nécessaire d'activer docker dans Administrer Jenkins > System Configuration :  
* Docker :  

![](https://i.imgur.com/Z4EabbA.png)


Ensuite, dans jenkins on crée un nouvelle item pipeline. Et on entre la configuration suivante :  
* GitLab Connection : 

![](https://i.imgur.com/5HE9brP.png)


* Buid Trigger: 

![](https://i.imgur.com/RNDNsnc.png)


* Pipeline:

![](https://i.imgur.com/HF92slQ.png)








## 5) Serveur NFS

### Ajout de volume

Ajout de 3 volume (8Go), créés avec amazon EBS puis rattachés a l'instance 
du serveur NFS, pour pouvoir mettre en place un RAID5 pouvant tolérer une 
panne et assurer une haute disponibilité

*Zone d'hébergement des volumes sur AWS: eu-south-1b*

Création d'un RAID5:

```sh
sudo mdadm --create /dev/md1 --level=5 --raid-devices=3 /dev/nvme1n1 
/dev/nvme2n1 /dev/nvme3n1 
```


```sh
pvcreate /dev/md1
vgcreate vg1 /dev/md1
lvcreate -l  80%VG -n lv1 vg1
```
On a choisi d'utiliser le format xfs qui est optimisé pour de gros volumes 
et permet le redimensionnement a chaud

```sh
sudo mkfs -t xfs /dev/vg1/lv1
```
---



### Roles de mise en place du serveur nfs

On installe les packages nécessaires sur le serveur NFS, et on s'assure que 
les services sont démarrés.
Puis on créé le dossier qui sera partagé avec les clients NFS.

```yaml
---
# tasks file for nfs
    - name: installation packages
      yum:
        name: 
          - nfs-utils
        state: present
        
    - name: Give writable mode to http folder
      file:
        path: /nfs-share
        state: directory
        mode: '0755'


    - name: start et enable les services
      service:
        name: rpcbind
        state: started
        enabled: true

    - name: start et enable les services
      service:
        name: nfs-server
        state: started
        enabled: true

    - name: start et enable les services
      service:
        name: nfs-lock
        state: started
        enabled: true

    - name: start et enable les services
      service:
        name:  nfs-idmap 
        state: started
        enabled: true

...
```

#### Sur le serveur nfs, on configure le dossier qui sera partagé.

Pour cela on édite un fichier /etc/exports dans lequel on ajoute le dossier 
a partager, l'adresse ip du client nfs et les options qui y seront 
rattachées:

```sh
/nfs-share/exports/server_appli 
15.161.53.65(rw,sync,no_root_squash,no_all_squash)
/nfs-share/exports/server_outils 
15.161.97.81(rw,sync,no_root_squash,no_all_squash)
```
On redemarre le service NFS
```sh
sudo systemctl restart nfs-server
```
Puis on partage ce dossier aux clients NFS:
```sh
sudo exportfs
```
---
### playbook configuration nfs sur les machines clientes

On installe le package nécessaire au fonctionnement de NFS sur les clients.

*centos: nfs-utils*
*debian: nfs-common*

Puis on monte le volume NFS, faisant le lien entre les dossiers qui seront 
partagés et le point de montage du serveur NFS

Sur le serveur applications:

```yaml
---

- name: mise en place des fichiers partagés nfs sur les clients
  hosts: server_service
  become: true
  remote_user: centos
  tasks:
    - name: installation nfs-utils
      yum:
        name: nfs-utils
        state: present

    - name: montage volume NFS
      mount:
        src: 15.161.108.88:/nfs-share/exports/server_appli
        path: /srv
        opts: rw,sync,hard,intr
        state: mounted
        fstype: nfs
...
```

Sur le serveur outils:

```yaml
---
- name: mise en place des fichiers partagés nfs sur les clients
  hosts: server_outils
  become: true
  remote_user: admin
  tasks:
    - name: installation nfs-common
      apt:
        name: nfs-common
        state: present

    - name: montage volume NFS
      mount:
        src: 15.161.108.88:/nfs-share/exports/server_outils
        path: /srv
        opts: rw,sync,hard,intr
        state: mounted
        fstype: nfs
```
Une fois le service NFS operationnel, les fichiers enregistrés dans /srv/ 
sur les serveurs applications et outils seront également enregistrés sur le 
serveur NFS /nfs-share/

---

## 6) Création d'un reverse proxy

Un serveur proxy va servir d'intermédiaire , il va recevoir les requêtes clients et les transmettre aux applications placées en backend. Le client n'aura pas accès directement aux applications.

### a) Déploiement du reverse proxy 

Pour mettre en place notre reverse proxy, nous allons passer par notre ansible. 
Dans notre dossier roles, nous allons créer un dossier reverse_proxy qui contiendra deux autres dossiers, files et tasks.
Le dossier files, va contenir tous les fichiers .conf correspondant aux services que nous pourrions utiliser.
Ces services sont gitlab, httpd, jenkins, phpadmin et tomcat.

La disposition des fichiers .conf est la même, elle varie légèrement en fonction du service demandé.


* Gitlab
```
server {
	listen 80;
	
  server_name www.gitlab.Groupe3_AstridCyliaRobinColas.com;

  location / {
      proxy_set_header HOST $host;
      proxy_set_header X-Forwarded-Proto $scheme;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_pass $scheme://15.161.97.81;
  }
}

```

* Httpd
```
server {
	listen 80;
	
  server_name www.jekyll.com;

  location / {
      proxy_set_header HOST $host;
      proxy_set_header X-Forwarded-Proto $scheme;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_pass $scheme://15.161.53.65/;
  }
}

```

* Jenkins
```
server {
	listen 80;
	
  server_name www.jenkins.com;

  location / {
      proxy_set_header HOST $host;
      proxy_set_header X-Forwarded-Proto $scheme;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_pass $scheme://15.161.97.81:8080;
  }
}

```

* Phpadmin
```
server {
	listen 80;
	
  server_name www.phpadmin.com;

  location / {
      proxy_set_header HOST $host;
      proxy_set_header X-Forwarded-Proto $scheme;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_pass $scheme://15.161.53.65:8081;
  }
}

```

* Tomcat
```
server {
	listen 80;
	
  server_name www.tomcat.com;

  location / {
      proxy_set_header HOST $host;
      proxy_set_header X-Forwarded-Proto $scheme;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_pass $scheme://15.161.53.65:8082;
  }
}

```

Dans ce fichier, on ouvre le bloc server, c'est l'équivalent d'un virtualhost, il va nous permettre de mettre nos configurations de site.
Il va écouter le port 80, et le server name va créer le nom de domaine.
Location va nous permettre de mettre les configurations de sécurité plus précise, à quel vitual host s'adresser et les directives du proxy web.

Pour l'ensemble des fichiers, il faut changer la ligne du server_name et mettre le service correspondant et changer le port qui suit l'addresse ip pour ne pas créer de conflit de port.

Une fois avoir créé tous les fichiers .conf, on peut aller dans le dossier task qui contient le fichier main.yml.

```yaml=
---
# tasks file for reverse_proxy
- name: install nginx 
  apt:
    name: nginx 
    state: present
    update_cache: yes


- name: deploy gitlab NGINX configuration
  copy: src=gitlab.conf dest=/etc/nginx/sites-available/gitlab.conf owner=root group=root mode=0644

- name: Enable gitlab NGINX configuration
  file: src=/etc/nginx/sites-available/gitlab.conf dest=/etc/nginx/sites-enabled/gitlab.conf state=link

- name: deploy jenkins NGINX configuration
  copy: src=jenkins.conf dest=/etc/nginx/sites-available/jenkins.conf owner=root group=root mode=0644

- name: Enable jenkins NGINX configuration
  file: src=/etc/nginx/sites-available/jenkins.conf dest=/etc/nginx/sites-enabled/jenkins.conf state=link

- name: deploy tomcat NGINX configuration
  copy: src=tomcat.conf dest=/etc/nginx/sites-available/tomcat.conf owner=root group=root mode=0644

- name: Enable tomcat NGINX configuration
  file: src=/etc/nginx/sites-available/tomcat.conf dest=/etc/nginx/sites-enabled/tomcat.conf state=link

- name: deploy phpadmin NGINX configuration
  copy: src=phpadmin.conf dest=/etc/nginx/sites-available/phpadmin.conf owner=root group=root mode=0644

- name: Enable phpadmin NGINX configuration
  file: src=/etc/nginx/sites-available/phpadmin.conf dest=/etc/nginx/sites-enabled/phpadmin.conf state=link

- name: deploy httpd NGINX configuration
  copy: src=httpd.conf dest=/etc/nginx/sites-available/httpd.conf owner=root group=root mode=0644

- name: Enable httpd NGINX configuration
  file: src=/etc/nginx/sites-available/httpd.conf dest=/etc/nginx/sites-enabled/httpd.conf state=link

- name: Restart NGINX
  service: name=nginx state=restarted

```
Dans ce fichier, dans un premier temps, il vérifie la présence de Nginx, l'installe et procède à sa mise à jour, si besoin.

Puis après, il va réaliser deux tâches par service. La première sert à déployer le service en copiant le fichier .conf qui correspond. Il précise le nom de la tâche en première ligne. Puis en seconde ligne, le fichier et la destination, le proprétaire, le groupe du fichier et les règles de restriction.

La seconde tâche va servir à activer le service. En première ligne, de nouveau le nom de la tâche. Puis en seconde ligne, le fichier, la destination et le lien symbolique mi en place pour connecter le site.

Après la fin des déploiement et la conection des services.
On fait un dernière têche qui va demander de rédémarrer le service Nginx.

Il faut lancer le playbook frontend avec la commande ci dessous, la mise en place du proxy reverse est effectué.

```
ansible-playbook -i hosts playbook_frontend.yml --key-file /root/.ssh/aws.key_frontend

```



### b) Visualition des pages des applications.

Pour afficher les pages Tomcat, Jenkins, Phpadmin et Gitlab dans notre navigateur web, nous devons effectuer un dernier changement directement sur notre propre pc.

Sur un pc windows, il faut ouvrir le fichier hosts. on peut changer ce fichier en passant par le chemin suivant:

C:\Windows\System32\drivers\etc

A ce fichier hosts, il suffit juste de rajouter à la fin du fichier :

```
35.152.95.31 www.gitlab.Groupe3_AstridCyliaRobinColas.com
35.152.95.31 www.jenkins.com
35.152.95.31 www.tomcat.com
35.152.95.31 www.phpadmin.com
35.152.95.31 www.jekyll.com
```

On enregistre le fichier une fois celui-ci modifié. Il se peut que vous n'alliez pas la possibilité d'enregistrer directement le fichier dans le dossier etc. 
Il faut alors l'enregistrer dans document avec une extension en .txt, puis glisser le fichier dans le dossier etc. Ensuite on supprime l'ancien fichier hosts et on modifie notre nouveau fichier en enlevant le .txt.

On peut enfin aller sur notre navigateur web et afficher l'ensemble de nos applications.














---SAUVEGARDE AU CAS OU---
## 3) création d’un compte Gitlab
* Dans visual studio code, il faut mettre:
	 * `git init`
	 * `git config --global user.name “groupe3 AstridCyliaRobinColas”`
	 * `git config --global user.email “user.email@mail.com”`
	 * `git remote add origin https://gitlab.com/g2692/projetfinal.git`
	 * `git clone https://gitlab.com/g2692/projetfinal.git `

* Pour récupérer le travail effectué:
	 * `git remote -v`
	 * `git fetch https://gitlab.com/g2692/projet.final.git`
	 * `git pull https://gitlab.com/g2692/projet.final.git`

## 4) Création des différents éléments pour Ansible:
* On doit installer ansible sur la vm, ici une centos 8:
	 * `sudo yum install epel-release`
	 * `sudo yum install ansible`
* Ensuite, il faut créer et paramétrer en codant :
	 * Dans le fichier host, il faut indiquer les adresses des différentes vm pour les créer.
	 * créer le fichier playbook_aws.yml
#### ?) Installation de self-managed GitLab




















